package com.design.principles.dependenceInversion.after;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/10/8 22:44
 * @since JDK1.8
 */
public class ComputerDemo {
    public static void main(String[] args) {
        //创建自建对象
        HardDisk hardDisk = new XiJieHardDisk();
//        Cpu cpu = new IntelCpu();
        Cpu cpu = new AmdCpu();
        Memory memory = new KingstonMemory();

        //创建计算机对象
        Computer c = new Computer();
        //组装计算机
        c.setHardDisk(hardDisk);
        c.setCpu(cpu);
        c.setMemory(memory);
        //运行计算机
        c.run();
    }
}
