package com.design.principles.dependenceInversion.after;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/10/8 22:48
 * @since JDK1.8
 */
public class AmdCpu implements Cpu {
    @Override
    public void run() {
        System.out.println("使用AMD处理器");
    }
}
