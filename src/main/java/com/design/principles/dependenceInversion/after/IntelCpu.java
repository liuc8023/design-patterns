package com.design.principles.dependenceInversion.after;

/**
 * Intel Cpu
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/8 22:20
 * @since JDK1.8
 */
public class IntelCpu implements Cpu {
    @Override
    public void run() {
        System.out.println("使用Intel处理器");
    }
}
