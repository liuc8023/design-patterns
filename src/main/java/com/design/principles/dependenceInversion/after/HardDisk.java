package com.design.principles.dependenceInversion.after;

/**
 * 硬盘接口
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/8 22:35
 * @since JDK1.8
 */
public interface HardDisk {
    /**
     * 存储数据
     *
     * @param data
     * @return void
     * @throws
     * @author liuc
     * @date 2021/10/8 22:36
     */
    public void save(String data);

    public String get();
}
