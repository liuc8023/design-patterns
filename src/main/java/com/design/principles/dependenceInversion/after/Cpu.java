package com.design.principles.dependenceInversion.after;

/**
 * Cpu接口
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/8 22:38
 * @since JDK1.8
 */
public interface Cpu {
    /**
     * 运行cpu
     *
     * @param
     * @return void
     * @throws
     * @author liuc
     * @date 2021/10/8 22:38
     */
    public void run();
}
