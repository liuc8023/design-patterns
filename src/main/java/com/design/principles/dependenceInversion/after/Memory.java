package com.design.principles.dependenceInversion.after;

/**
 * 内存条接口
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/8 22:39
 * @since JDK1.8
 */
public interface Memory {
    /**
     * 使用内存条
     *
     * @param
     * @return void
     * @throws
     * @author liuc
     * @date 2021/10/8 22:40
     */
    public void save();
}
