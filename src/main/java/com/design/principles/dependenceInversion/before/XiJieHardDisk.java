package com.design.principles.dependenceInversion.before;

/**
 * 希捷硬盘
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/8 22:16
 * @since JDK1.8
 */
public class XiJieHardDisk {
    /**
     * 存储数据的方法
     *
     * @param data
     * @return void
     * @throws
     * @author liuc
     * @date 2021/10/8 22:17
     */
    public void save(String data) {
        System.out.println("使用希捷硬盘存储数据为：" + data);
    }

    /**
     * 获取数据的方法
     *
     * @param
     * @return java.lang.String
     * @throws
     * @author liuc
     * @date 2021/10/8 22:19
     */
    public String get() {
        System.out.println("使用希捷硬盘获取数据");
        return "数据";
    }
}
