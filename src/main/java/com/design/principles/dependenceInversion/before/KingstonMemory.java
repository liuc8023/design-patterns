package com.design.principles.dependenceInversion.before;

/**
 * 金士顿内存条类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/8 22:22
 * @since JDK1.8
 */
public class KingstonMemory {
    public void save() {
        System.out.println("使用金士顿内存条");
    }
}
