package com.design.principles.liskovSubstitution.before;

/**
 * @author liuc
 * @version V1.0
 * @description 长方形类
 * @date 2021/9/26 21:42
 * @since JDK1.8
 */
public class Rectangle {
    private double length;
    private double width;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
