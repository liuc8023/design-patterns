package com.design.principles.liskovSubstitution.before;

/**
 * @author liuc
 * @version V1.0
 * @description 正方形类
 * @date 2021/9/26 21:46
 * @since JDK1.8
 */
public class Square extends Rectangle {
    @Override
    public void setLength(double length) {
        super.setLength(length);
        super.setWidth(length);
    }

    @Override
    public void setWidth(double width) {
        super.setLength(width);
        super.setWidth(width);
    }
}
