package com.design.principles.liskovSubstitution.after;

/**
 * @author liuc
 * @version V1.0
 * @description 正方形类
 * @date 2021/9/26 22:08
 * @since JDK1.8
 */
public class Square implements Quadrilateral {
    private double side;

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double getLength() {
        return side;
    }

    @Override
    public double getWidth() {
        return side;
    }
}
