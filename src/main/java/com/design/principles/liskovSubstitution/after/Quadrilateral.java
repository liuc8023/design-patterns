package com.design.principles.liskovSubstitution.after;

/**
 * @author liuc
 * @version V1.0
 * @description 四边形接口
 * @date 2021/9/26 22:07
 * @since JDK1.8
 */
public interface Quadrilateral {
    /**
     * 获取长
     *
     * @param
     * @return
     * @throws
     * @author liuc
     * @date 2021/9/27 14:54
     */
    double getLength();

    /**
     * 获取宽
     *
     * @param
     * @return
     * @throws
     * @author liuc
     * @date 2021/9/27 14:55
     */
    double getWidth();
}
