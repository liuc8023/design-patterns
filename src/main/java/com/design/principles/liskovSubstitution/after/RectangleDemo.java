package com.design.principles.liskovSubstitution.after;

/**
 * 里氏替换原则测试类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/9/28 11:02
 * @since JDK1.8
 */
public class RectangleDemo {
    public static void main(String[] args) {
        //创建长方形对象
        Rectangle r = new Rectangle();
        r.setLength(20);
        r.setWidth(10);
        //调用resize方法进行扩宽
        resize(r);
        printLengthAndWidth(r);
        System.out.println("================");
        //创建正方形对象
        Square s = new Square();
        //设置长和宽
        s.setSide(10);
        printLengthAndWidth(s);
    }

    /**
     * 扩展方法
     *
     * @param rectangle
     * @return void
     * @throws
     * @author liuc
     * @date 2021/9/28 11:00
     */
    public static void resize(Rectangle rectangle) {
        //判断宽如果比长小，进行扩宽的操作
        while (rectangle.getWidth() <= rectangle.getLength()) {
            rectangle.setWidth(rectangle.getWidth() + 1);
        }
    }

    /**
     * 打印长和宽
     *
     * @param quadrilateral
     * @return void
     * @throws
     * @author liuc
     * @date 2021/9/28 11:01
     */
    public static void printLengthAndWidth(Quadrilateral quadrilateral) {
        System.out.println(quadrilateral.getLength());
        System.out.println(quadrilateral.getWidth());
    }
}
