package com.design.principles.liskovSubstitution.after;

/**
 * @author liuc
 * @version V1.0
 * @description 长方形类
 * @date 2021/9/26 22:10
 * @since JDK1.8
 */
public class Rectangle implements Quadrilateral {
    private double length;
    private double width;

    @Override
    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
