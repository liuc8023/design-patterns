package com.design.principles.leastKnowledge;

/**
 * 媒体公司类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 20:19
 * @since JDK1.8
 */
public class Company {
    private String name;

    public Company(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
