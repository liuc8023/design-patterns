package com.design.principles.leastKnowledge;

/**
 * 粉丝类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 20:17
 * @since JDK1.8
 */
public class Fans {
    private String name;

    public Fans(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
