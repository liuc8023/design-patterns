package com.design.principles.leastKnowledge;

/**
 * 迪米特法则测试类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 20:21
 * @since JDK1.8
 */
public class LoDtest {
    public static void main(String[] args) {
        Agent agent = new Agent();
        agent.setStar(new Star("林心如"));
        agent.setFans(new Fans("粉丝韩丞"));
        agent.setCompany(new Company("中国传媒有限公司"));
        agent.meeting();
        agent.business();
    }
}
