package com.design.principles.leastKnowledge;

/**
 * 明星类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 20:13
 * @since JDK1.8
 */
public class Star {
    private String name;

    public Star(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
