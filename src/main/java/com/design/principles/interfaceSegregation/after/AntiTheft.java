package com.design.principles.interfaceSegregation.after;

/**
 * 防盗接口
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 14:39
 * @since JDK1.8
 */
public interface AntiTheft {
    /**
     * 防盗
     *
     * @param
     * @return void
     * @throws
     * @author liuc
     * @date 2021/10/10 14:30
     */
    void antiTheft();
}
