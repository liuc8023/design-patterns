package com.design.principles.interfaceSegregation.after;

/**
 * 防水接口
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 14:40
 * @since JDK1.8
 */
public interface WaterProof {
    /**
     * 防水
     *
     * @param
     * @return void
     * @throws
     * @author liuc
     * @date 2021/10/10 14:31
     */
    void waterProof();
}
