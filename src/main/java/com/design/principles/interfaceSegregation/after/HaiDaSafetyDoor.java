package com.design.principles.interfaceSegregation.after;

/**
 * 海达安全门
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 14:43
 * @since JDK1.8
 */
public class HaiDaSafetyDoor implements AntiTheft, FireProof {

    @Override
    public void antiTheft() {
        System.out.println("防盗");
    }

    @Override
    public void fireProof() {
        System.out.println("防火");
    }
}
