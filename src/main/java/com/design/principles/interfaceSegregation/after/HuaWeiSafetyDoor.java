package com.design.principles.interfaceSegregation.after;


/**
 * 华威安全门类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 14:32
 * @since JDK1.8
 */
public class HuaWeiSafetyDoor implements AntiTheft, FireProof, WaterProof {

    @Override
    public void antiTheft() {
        System.out.println("防盗");
    }

    @Override
    public void fireProof() {
        System.out.println("防火");
    }

    @Override
    public void waterProof() {
        System.out.println("防水");
    }
}
