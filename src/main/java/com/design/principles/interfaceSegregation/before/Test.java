package com.design.principles.interfaceSegregation.before;

/**
 * 测试类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 14:36
 * @since JDK1.8
 */
public class Test {
    public static void main(String[] args) {
        HuaWeiSafetyDoor door = new HuaWeiSafetyDoor();
        door.antiTheft();
        door.fireProof();
        door.waterProof();
    }
}
