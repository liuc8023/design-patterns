package com.design.principles.interfaceSegregation.before;

/**
 * 安全门接口
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 14:29
 * @since JDK1.8
 */
public interface SafetyDoor {
    /**
     * 防盗
     *
     * @param
     * @return void
     * @throws
     * @author liuc
     * @date 2021/10/10 14:30
     */
    void antiTheft();

    /**
     * 防火
     *
     * @param
     * @return void
     * @throws
     * @author liuc
     * @date 2021/10/10 14:30
     */
    void fireProof();

    /**
     * 防水
     *
     * @param
     * @return void
     * @throws
     * @author liuc
     * @date 2021/10/10 14:31
     */
    void waterProof();
}
