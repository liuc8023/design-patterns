package com.design.principles.openClosed;

/**
 * @author liuc
 * @version V1.0
 * @description 炫耀星空皮肤类
 * @date 2021/9/25 23:17
 * @since JDK1.8
 */
public class StarryNightSkySkin extends AbstractSkin {
    @Override
    public void display() {
        System.out.println("炫耀星空皮肤");
    }
}
