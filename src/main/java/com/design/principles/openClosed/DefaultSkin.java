package com.design.principles.openClosed;

/**
 * @author liuc
 * @version V1.0
 * @description 默认皮肤类
 * @date 2021/9/25 23:14
 * @since JDK1.8
 */
public class DefaultSkin extends AbstractSkin {
    @Override
    public void display() {
        System.out.println("默认皮肤");
    }
}
