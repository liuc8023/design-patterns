package com.design.principles.openClosed;

/**
 * @author liuc
 * @version V1.0
 * @description 抽象皮肤类
 * @date 2021/9/25 23:12
 * @since JDK1.8
 */
public abstract class AbstractSkin {
    public abstract void display();
}
