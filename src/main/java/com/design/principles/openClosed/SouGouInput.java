package com.design.principles.openClosed;

/**
 * @author liuc
 * @version V1.0
 * @description 搜狗输入法
 * @date 2021/9/25 23:19
 * @since JDK1.8
 */
public class SouGouInput {
    private AbstractSkin skin;

    public void setSkin(AbstractSkin skin) {
        this.skin = skin;
    }

    public void display() {
        skin.display();
    }
}
