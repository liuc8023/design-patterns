package com.design.principles.openClosed;

/**
 * @author liuc
 * @version V1.0
 * @description
 * @date 2021/9/25 23:23
 * @since JDK1.8
 */
public class Client {
    public static void main(String[] args) {
        //1、创建搜狗输入法对象
        SouGouInput input = new SouGouInput();
        //2、创建皮肤对象
        // DefaultSkin skin = new DefaultSkin();
        StarryNightSkySkin skin = new StarryNightSkySkin();
        //3、将皮肤设置到输入法中
        input.setSkin(skin);
        //4、显示皮肤
        input.display();
    }
}
