package com.design.creational.prototype;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 原型模式
 */
public class Person implements Cloneable,Serializable{

    public Person (){
        System.out.println("构造函数调用");
    }
    private String name;
    private int age;
    private List<String> list = new ArrayList<String>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    @Override
    protected Person clone() throws CloneNotSupportedException {
        return (Person) super.clone();
    }

    /**
     * 深拷贝
     * @return
     */
    public Object deepClone(){
        Person p = null;
        try {
            //序列化输出
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(this);

            //序列化输入
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            p =  (Person)ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p;
    }
    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", list=" + list +
                '}';
    }
}
