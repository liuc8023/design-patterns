package com.design.creational.prototype;

public class Main {
    public static void main(String[] args) {
        Person p = new Person();
        p.setName("张三");
        p.setAge(22);
        p.getList().add("aaa");
        Person p2 = null;
        try {
            /**
             * 浅拷贝时，p和p2的成员变量指向相同的内存地址
             * 执行结果是：
             *   p对象：Person{name='张三', age=22, list=[aaa, ccc]}
             *   p2对象：Person{name='李四', age=22, list=[aaa, ccc]}
             */
//            p2 = (Person) p.clone();
            /**
             * 深拷贝时，通过序列化的方式将p和p2的成员变量（基本类型数据或者引用类型数据）指向不同的内存地址实现拷贝，使得拷贝后的对象
             * 修改其成员变量里的数据不会对被拷贝对象产生影响
             * 执行结果是：
             *   p对象：Person{name='张三', age=22, list=[aaa]}
             *   p2对象：Person{name='李四', age=22, list=[aaa, ccc]}
             */
            p2 = (Person) p.deepClone();
            p2.setName("李四");
            p2.getList().add("ccc");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("p对象："+p);
        System.out.println("p2对象："+p2);
    }
}
