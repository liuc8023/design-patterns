package com.design.creational.prototype.reconsitution;

import java.util.Map;

/**
 * 选择题题库类
 * @author liuc
 */
public class Topic {
    /**
     * 选项；A、B、C、D
     */
    private Map<String, String> option;
    /**
     * 答案；B
     */
    private String answer;

    public Topic() {
    }

    public Topic(Map<String, String> option, String answer) {
        this.option = option;
        this.answer = answer;
    }

    public Map<String, String> getOption() {
        return option;
    }

    public void setOption(Map<String, String> option) {
        this.option = option;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
