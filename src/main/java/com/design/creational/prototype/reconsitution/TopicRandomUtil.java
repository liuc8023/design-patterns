package com.design.creational.prototype.reconsitution;

import java.util.*;

/**
 * 题目选项乱序操作工具包
 * @author liuc
 */
public class TopicRandomUtil {
    static public Topic random(Map<String, String> option, String answer) {
        Set<String> keySet = option.keySet();
        ArrayList<String> keyList = new ArrayList<String>(keySet);
        Collections.shuffle(keyList);
        HashMap<String, String> optionNew = new HashMap<String, String>();
        int idx = 0;
        String keyNew = "";
        for (String next : keySet) {
            String randomKey = keyList.get(idx++);
            if (answer.equals(next)) {
                keyNew = randomKey;
            }
            optionNew.put(randomKey, option.get(next));
        }
        return new Topic(optionNew, keyNew);
    }
}
