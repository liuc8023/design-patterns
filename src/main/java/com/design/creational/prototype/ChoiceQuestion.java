package com.design.creational.prototype;

import java.io.Serializable;
import java.util.Map;

/**
 * 单选题
 * @author liuc
 */
public class ChoiceQuestion implements Serializable {
    /**
     * 题目
     */
    private String topic;
    /**
     * 选项；A、B、C、D
     */
    private Map<String,String> option;
    /**
     * 答案
     */
    private String answer;

    public ChoiceQuestion(String topic, Map<String, String> option, String answer) {
        this.topic = topic;
        this.option = option;
        this.answer = answer;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Map<String, String> getOption() {
        return option;
    }

    public void setOption(Map<String, String> option) {
        this.option = option;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
