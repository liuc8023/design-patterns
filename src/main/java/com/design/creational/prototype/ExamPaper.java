package com.design.creational.prototype;

/**
 * 测试类
 * @author liuc
 */
public class ExamPaper {
    public static void main(String[] args) {
        QuestionBankController questionBankController = new QuestionBankController();
        System.out.println(questionBankController.createPaper("花花", "1000001921032"));
        System.out.println(questionBankController.createPaper("豆豆", "1000001921051"));
        System.out.println(questionBankController.createPaper("大宝", "1000001921987"));
    }
}
