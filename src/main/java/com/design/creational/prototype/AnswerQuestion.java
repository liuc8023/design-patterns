package com.design.creational.prototype;

import java.io.Serializable;

/**
 * 问答题
 * @author liuc
 */
public class AnswerQuestion implements Serializable {
    /**
     * 问题
     */
    private String answer;
    /**
     * 答案
     */
    private String question;

    public AnswerQuestion(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
