package com.design.creational.builder;

/**
 * 申明建造者的公共方法
 */
public interface ComputerBuilder {
    void buildCpu();
    void buildMainBoard();
    void buildDisk();
    void buildPower();
    void buildMemory();

    Computer createComputer();
}
