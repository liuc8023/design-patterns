package com.design.creational.builder;

public class Main {
    public static void main(String[] args) {
        ComputerDirector director = new ComputerDirector();
        Computer lowComputer = director.create(new LowComputerBuilder());
        Computer highComputer = director.create(new HighComputerBuilder());
        System.out.println(lowComputer);
        System.out.println(highComputer);
    }
}
