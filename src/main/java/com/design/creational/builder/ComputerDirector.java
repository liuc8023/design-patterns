package com.design.creational.builder;

/**
 * 将产品和创建过程进行解耦，使用相同的创建过程创建不同的产品，控制产品生产过程
 * Director是全程指导组装过程，具体的细节还是builder去操作
 */
public class ComputerDirector {
    public Computer create(ComputerBuilder builder){
        builder.buildCpu();
        builder.buildPower();
        builder.buildDisk();
        builder.buildMemory();
        builder.buildMainBoard();
        return builder.createComputer();
    }
}
