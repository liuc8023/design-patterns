package com.design.creational.factory.simpleFactory;

/**
 * 简单工厂，既能生产宝马又能生产奔驰
 *
 * @author liuc
 * @version V1.0
 * @date 2021/9/29 16:52
 * @since JDK1.8
 */
public class SimpleFactory {
    public Car getCar(String name) {
        if (name.equals("Benz")) {
            return new Benz();
        } else if (name.equals("BMW")) {
            return new BMW();
        } else {
            System.out.println("不好意思，这个品牌的汽车生产不了");
            return null;
        }
    }
}
