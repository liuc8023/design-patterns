package com.design.creational.factory.simpleFactory;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/9/29 16:55
 * @since JDK1.8
 */
public class SimpleFactoryTest {

    public static void main(String[] args) {
        SimpleFactory factory = new SimpleFactory();
        Car car = factory.getCar("BMW");
        System.out.println(car.getName());
    }
}
