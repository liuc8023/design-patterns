package com.design.creational.factory.simpleFactory;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/9/29 16:48
 * @since JDK1.8
 */
public interface Car {
    String getName();
}
