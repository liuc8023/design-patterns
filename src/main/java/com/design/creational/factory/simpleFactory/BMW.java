package com.design.creational.factory.simpleFactory;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/9/29 16:51
 * @since JDK1.8
 */
public class BMW implements Car {
    @Override
    public String getName() {
        return "生产BMW车中。。。";
    }
}
