package com.design.creational.factory.simpleFactory.pay;

public class Main {
    public static void main(String[] args) {
        Pay payFactory= SimplePayFactory.createPay("ALI_PAY");
        payFactory.unifiedorder();
    }
}
