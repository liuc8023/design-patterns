package com.design.creational.factory.simpleFactory.pay;

public class SimplePayFactory {
    /**
     * 根据参数返回对应的支付对象
     * @param payType
     * @return
     */
    public static Pay createPay(String payType){
        if (payType == null) {
            return null;
        } else if ("WECHAT_PAY".equalsIgnoreCase(payType)) {
            return new WechatPay();
        } else if ("ALI_PAY".equalsIgnoreCase(payType)) {
            return new AliPay();
        }
        return null;
    }
}
