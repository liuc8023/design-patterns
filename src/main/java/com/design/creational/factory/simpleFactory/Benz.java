package com.design.creational.factory.simpleFactory;

/**
 * 奔驰类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/9/29 16:49
 * @since JDK1.8
 */
public class Benz implements Car {
    @Override
    public String getName() {
        return "生产Benz车中。。。";
    }
}
