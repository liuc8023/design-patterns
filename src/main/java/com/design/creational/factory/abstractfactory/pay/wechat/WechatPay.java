package com.design.creational.factory.abstractfactory.pay.wechat;

import com.design.creational.factory.abstractfactory.pay.PayFactory;

public class WechatPay implements PayFactory {
    @Override
    public void unifiedorder() {
        System.out.println("微信支付 统一下单接口");
    }
}
