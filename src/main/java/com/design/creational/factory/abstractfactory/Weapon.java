package com.design.creational.factory.abstractfactory;

public abstract class Weapon {
    abstract void shoot();
}
