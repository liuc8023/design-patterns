package com.design.creational.factory.abstractfactory.pay.ali;

import com.design.creational.factory.abstractfactory.pay.AbstractOrderFactory;
import com.design.creational.factory.abstractfactory.pay.PayFactory;
import com.design.creational.factory.abstractfactory.pay.RefundFactory;

public class AliOrderFactory extends AbstractOrderFactory {
    public static AliOrderFactory INSTANCE = new AliOrderFactory();
    private AliOrderFactory(){}
    public static AliOrderFactory getInstance(){
        return INSTANCE;
    }
    @Override
    public PayFactory createPay() {
        return new AliPay();
    }

    @Override
    public RefundFactory createRefund() {
        return new AliRefund();
    }
}
