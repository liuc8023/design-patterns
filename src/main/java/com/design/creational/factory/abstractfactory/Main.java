package com.design.creational.factory.abstractfactory;

public class Main {
    public static void main(String[] args) {
        AbstractFactory f = new MagicFactory();
        Vehicle v = f.createVehicle();
        v.go();
        Weapon w = f.createWeapon();
        w.shoot();
        Food fo = f.createFood();
        fo.printName();
        Car c = new Car();
    }
}
