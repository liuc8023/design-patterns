package com.design.creational.factory.abstractfactory.pay.wechat;

import com.design.creational.factory.abstractfactory.pay.RefundFactory;

public class WechatRefund implements RefundFactory {
    @Override
    public void unifiedRefund() {
        System.out.println("微信支付 统一退款接口");
    }
}
