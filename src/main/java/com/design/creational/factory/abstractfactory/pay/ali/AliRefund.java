package com.design.creational.factory.abstractfactory.pay.ali;

import com.design.creational.factory.abstractfactory.pay.RefundFactory;

public class AliRefund implements RefundFactory {
    @Override
    public void unifiedRefund() {
        System.out.println("支付宝支付 统一退款接口");
    }
}
