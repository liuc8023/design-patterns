package com.design.creational.factory.abstractfactory.pay;

public interface RefundFactory {
    /**
     * 统一退款方法
     */
    void unifiedRefund();
}
