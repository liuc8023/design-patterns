package com.design.creational.factory.abstractfactory.pay.ali;

import com.design.creational.factory.abstractfactory.pay.PayFactory;

public class AliPay implements PayFactory {
    @Override
    public void unifiedorder() {
        System.out.println("支付宝支付 统一下单接口");
    }
}
