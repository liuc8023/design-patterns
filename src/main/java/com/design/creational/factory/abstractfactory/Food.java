package com.design.creational.factory.abstractfactory;

public abstract class Food {
    abstract void printName();
}
