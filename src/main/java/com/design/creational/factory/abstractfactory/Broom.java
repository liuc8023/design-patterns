package com.design.creational.factory.abstractfactory;

public class Broom extends Vehicle{
    @Override
    public void go(){
        System.out.println("Broom flying sousousou.....");
    }
}
