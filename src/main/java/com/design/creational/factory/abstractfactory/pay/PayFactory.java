package com.design.creational.factory.abstractfactory.pay;

public interface PayFactory {
    /**
     * 统一下单
     */
    void unifiedorder();
}
