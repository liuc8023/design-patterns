package com.design.creational.factory.abstractfactory.pay.wechat;

import com.design.creational.factory.abstractfactory.pay.AbstractOrderFactory;
import com.design.creational.factory.abstractfactory.pay.PayFactory;
import com.design.creational.factory.abstractfactory.pay.RefundFactory;

public class WechatOrderFactory extends AbstractOrderFactory {
    public static WechatOrderFactory INSTANCE = new WechatOrderFactory();
    private WechatOrderFactory(){}
    public static WechatOrderFactory getInstance(){
        return INSTANCE;
    }
    @Override
    public PayFactory createPay() {
        return new WechatPay();
    }

    @Override
    public RefundFactory createRefund() {
        return new WechatRefund();
    }
}
