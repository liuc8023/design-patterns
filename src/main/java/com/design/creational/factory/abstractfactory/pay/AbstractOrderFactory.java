package com.design.creational.factory.abstractfactory.pay;

/**
 * 抽象订单工厂
 */
public abstract class AbstractOrderFactory {
    /**
     * 创建抽象支付工厂
     * @return
     */
    public abstract PayFactory createPay();

    /**
     * 创建抽象退款工程
     * @return
     */
    public abstract RefundFactory createRefund();
}
