package com.design.creational.factory.abstractfactory.pay;

import com.design.creational.factory.abstractfactory.pay.ali.AliOrderFactory;
import com.design.creational.factory.abstractfactory.pay.wechat.WechatOrderFactory;

public class Main {
    public static void main(String[] args) {
        AbstractOrderFactory aliOrderFactory = AliOrderFactory.getInstance();
        aliOrderFactory.createPay().unifiedorder();
        aliOrderFactory.createRefund().unifiedRefund();

        AbstractOrderFactory wechatOrderFactory = WechatOrderFactory.getInstance();
        wechatOrderFactory.createPay().unifiedorder();
        wechatOrderFactory.createRefund().unifiedRefund();
    }
}
