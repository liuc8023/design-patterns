package com.design.creational.factory.abstractfactory;

public abstract class Vehicle {
    abstract void go();
}
