package com.design.creational.factory.factoryMethod;

public class CarFactory {
    public Car create(){
        System.out.println("A Car created!");
        return new Car();
    }
}
