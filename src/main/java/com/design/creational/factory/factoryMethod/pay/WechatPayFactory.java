package com.design.creational.factory.factoryMethod.pay;

public class WechatPayFactory implements PayFactory{
    @Override
    public Pay getPay() {
        return new WechatPay();
    }
}
