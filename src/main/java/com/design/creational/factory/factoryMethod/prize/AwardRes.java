package com.design.creational.factory.factoryMethod.prize;

/**
 * 发奖结果反馈对象
 * @author liuc
 */
public class AwardRes {
    /**
     * 编码
     */
    private String code;
    /**
     * 描述
     */
    private String info;

    public AwardRes(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
