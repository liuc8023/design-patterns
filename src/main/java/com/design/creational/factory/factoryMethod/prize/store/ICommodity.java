package com.design.creational.factory.factoryMethod.prize.store;

import java.util.Map;

/**
 * 发放商品接口
 * @author liuc
 */
public interface ICommodity {
    void sendCommodity(String uId, String commodityId, String bizId, Map<String, String> extMap) throws Exception;
}
