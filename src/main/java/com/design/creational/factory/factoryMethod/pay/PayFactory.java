package com.design.creational.factory.factoryMethod.pay;

/**
 * 抽象工厂
 */
public interface PayFactory {
    Pay getPay();
}
