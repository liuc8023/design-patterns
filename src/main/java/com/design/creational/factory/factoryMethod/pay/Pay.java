package com.design.creational.factory.factoryMethod.pay;

public interface Pay {
    /**
     * 统一下单
     */
    void unifiedorder();
}
