package com.design.creational.factory.factoryMethod;

public interface Moveable {
    void go();
}
