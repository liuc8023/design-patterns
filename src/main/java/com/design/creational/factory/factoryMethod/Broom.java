package com.design.creational.factory.factoryMethod;

public class Broom implements Moveable{
    @Override
    public void go() {
        System.out.println("Broom flying chuachuachua...");
    }
}
