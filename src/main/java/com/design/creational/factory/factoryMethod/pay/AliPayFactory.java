package com.design.creational.factory.factoryMethod.pay;

public class AliPayFactory implements PayFactory{
    @Override
    public Pay getPay() {
        return new AliPay();
    }
}
