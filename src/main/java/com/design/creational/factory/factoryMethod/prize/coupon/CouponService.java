package com.design.creational.factory.factoryMethod.prize.coupon;

/**
 * 模拟优惠券服务
 * @author liuc
 */
public class CouponService {
    public CouponResult sendCoupon(String uId, String couponNumber, String uuid) {
        System.out.println("模拟发放优惠券一张：" + uId + "," + couponNumber + "," + uuid);
        return new CouponResult("0000", "发放成功");
    }
}
