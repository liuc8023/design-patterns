package com.design.creational.factory.factoryMethod.pay;

public class Main {
    public static void main(String[] args) {
        PayFactory aliPayFactory = new AliPayFactory();
        aliPayFactory.getPay().unifiedorder();
        PayFactory wechatPayFactory = new WechatPayFactory();
        wechatPayFactory.getPay().unifiedorder();
    }
}
