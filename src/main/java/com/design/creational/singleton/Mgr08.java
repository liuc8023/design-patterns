package com.design.creational.singleton;

/**
 * 枚举单例模式
 * 不仅可以解决线程同步问题，还可以防止反序列化。
 * 为什么枚举类可以防止反序列化？
 *    因为枚举类没有构造方法，所以没办法通过反射创建一个对象。
 * 此方式推荐
 */
public enum Mgr08 {
    INSTANCE;

    public void doSomething() {
        System.out.println("doSomething");
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                //1) 对象相等则hashCode一定相等；
                //2) hashCode相等对象未必相等。（不同对象的hashcode是否一定不一样？是错误的，
                // hashcode本身就是个函数，是可以重载的，你完全可以写个函数总是返回固定值。
                // 但hashcode函数从设计要求上来说，要尽量保证：不同对象的hashcode不同。）
                System.out.println(Mgr08.INSTANCE.hashCode());
                Mgr08.INSTANCE.doSomething();
            }).start();
        }
    }
}
