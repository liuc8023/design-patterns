package com.design.creational.singleton;

import java.util.concurrent.TimeUnit;

public class Mgr06 {
    //加上volatile的原因是防止指令重排的问题
    private static volatile Mgr06 INSTANCE;

    private Mgr06(){}

    public static Mgr06 getInstance(){
        if (INSTANCE == null){
            //双重检查锁
            synchronized (Mgr06.class){
                if (INSTANCE == null) {
                    try {
                        TimeUnit.MICROSECONDS.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    INSTANCE = new Mgr06();
                }
            }
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                //1) 对象相等则hashCode一定相等；
                //2) hashCode相等对象未必相等。（不同对象的hashcode是否一定不一样？是错误的，
                // hashcode本身就是个函数，是可以重载的，你完全可以写个函数总是返回固定值。
                // 但hashcode函数从设计要求上来说，要尽量保证：不同对象的hashcode不同。）
                System.out.println(Mgr06.getInstance().hashCode());
            }).start();
        }
    }
}
