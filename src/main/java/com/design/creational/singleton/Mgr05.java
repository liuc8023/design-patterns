package com.design.creational.singleton;

import java.util.concurrent.TimeUnit;

public class Mgr05 {
    private static Mgr05 INSTANCE;

    private Mgr05(){}

    public static Mgr05 getInstance(){
        if (INSTANCE == null){
            //妄图通过减小同步代码块的方式提高效率，然后不可行，线程是不安全的
            synchronized (Mgr05.class){
                try {
                    TimeUnit.MICROSECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                INSTANCE = new Mgr05();
            }
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                //1) 对象相等则hashCode一定相等;
                //2) hashCode相等对象未必相等。（不同对象的hashcode是否一定不一样？是错误的，
                // hashcode本身就是个函数，是可以重载的，你完全可以写个函数总是返回固定值。
                // 但hashcode函数从设计要求上来说，要尽量保证：不同对象的hashcode不同。）
                System.out.println(Mgr05.getInstance().hashCode());
            }).start();
        }
    }
}
