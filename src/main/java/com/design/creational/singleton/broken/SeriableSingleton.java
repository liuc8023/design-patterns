package com.design.creational.singleton.broken;

import java.io.Serializable;

/**
 * 可序列化的单例
 * @author liuc
 */
public class SeriableSingleton implements Serializable {
    //类初始化的时候边进行对象实例化
    private static final SeriableSingleton  seriableSingleton = new SeriableSingleton();
    private SeriableSingleton(){

    }
    public static SeriableSingleton getInstance() {
        return seriableSingleton;
    }

    /**
     * 只需要添加这一个函数即可解决序列化破坏单例对象的问题
     * @return
     */
    private Object readResolve(){
        return seriableSingleton;
    }
}
