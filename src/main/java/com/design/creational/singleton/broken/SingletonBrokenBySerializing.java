package com.design.creational.singleton.broken;

import java.io.*;

/**
 * 测试序列化破坏单例
 * @author liuc
 */
public class SingletonBrokenBySerializing {
    public static void main(String[] args) {
        SeriableSingleton s1 = SeriableSingleton.getInstance();
        SeriableSingleton s2 = null;
        FileOutputStream fos = null;
        try {
            File file;
            fos = new FileOutputStream("SeriableSingleton.obj");
            OutputStream out;
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(s1);
            oos.flush();
            oos.close();
            oos.close();
            FileInputStream fis = new FileInputStream("SeriableSingleton.obj");
            ObjectInputStream ois = new ObjectInputStream(fis);
            s2 = (SeriableSingleton)ois.readObject();
            ois.close();
            fis.close();
            //输出为false
            System.out.println(s1 == s2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
