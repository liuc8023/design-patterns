package com.design.creational.singleton;

import java.util.concurrent.TimeUnit;

/**
 * lazy loading
 * 懒汉式
 * 虽然达到了按需初始化的目的（即谁用谁初始化），但是却带来了线程不安全的问题，不推荐使用
 */
public class Mgr03 {
    private static Mgr03 INSTANCE;

    private Mgr03(){}

    public static Mgr03 getInstance(){
        if (INSTANCE == null){
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            INSTANCE = new Mgr03();
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                //1) 对象相等则hashCode一定相等；
                //2) hashCode相等对象未必相等。（不同对象的hashcode是否一定不一样？是错误的，
                // hashcode本身就是个函数，是可以重载的，你完全可以写个函数总是返回固定值。
                // 但hashcode函数从设计要求上来说，要尽量保证：不同对象的hashcode不同。）
                System.out.println(Mgr03.getInstance().hashCode());
            }).start();
        }
    }
}
