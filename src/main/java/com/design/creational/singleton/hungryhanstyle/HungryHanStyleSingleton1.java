package com.design.creational.singleton.hungryhanstyle;

/**
 * 饿汉式单例模式：在静态代码块中创建该类对象
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 21:48
 * @since JDK1.8
 */
public class HungryHanStyleSingleton1 {
    /**
     * 在成员位置创建该类的对象
     */
    private static HungryHanStyleSingleton1 instance;

    static {
        instance = new HungryHanStyleSingleton1();
    }

    /**
     * 私有构造方法
     *
     * @param
     * @return
     * @throws
     * @author liuc
     * @date 2021/10/10 21:38
     */
    private HungryHanStyleSingleton1() {
    }

    /**
     * 提供一个公共的访问方式，让外界获取该对象
     *
     * @param
     * @return com.design.creational.singleton.HungryHanStyleSingleton1
     * @throws
     * @author liuc
     * @date 2021/10/10 21:41
     */
    public static HungryHanStyleSingleton1 getInstance() {
        return instance;
    }
}
