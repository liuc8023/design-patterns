package com.design.creational.singleton.hungryhanstyle;

/**
 * 饿汉式单例模式
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 21:34
 * @since JDK1.8
 */
public class HungryHanStyleSingleton {
    /**
     * 在本类中创建本类对象
     */
    private static HungryHanStyleSingleton instance = new HungryHanStyleSingleton();

    /**
     * 私有构造方法
     *
     * @param
     * @return
     * @throws
     * @author liuc
     * @date 2021/10/10 21:38
     */
    private HungryHanStyleSingleton() {
    }

    /**
     * 提供一个公共的访问方式，让外界获取该对象
     *
     * @param
     * @return com.design.creational.singleton.HungryHanStyleSingleton
     * @throws
     * @author liuc
     * @date 2021/10/10 21:41
     */
    public static HungryHanStyleSingleton getInstance() {
        return instance;
    }
}
