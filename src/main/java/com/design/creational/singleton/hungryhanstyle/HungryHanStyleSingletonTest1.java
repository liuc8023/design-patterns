package com.design.creational.singleton.hungryhanstyle;

/**
 * 饿汉式单例模式：在静态代码块中创建该类对象测试类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 22:00
 * @since JDK1.8
 */
public class HungryHanStyleSingletonTest1 {
    public static void main(String[] args) {
        //创建HungryHanStyleSingleton1类的对象
        HungryHanStyleSingleton1 instance = HungryHanStyleSingleton1.getInstance();
        HungryHanStyleSingleton1 instance1 = HungryHanStyleSingleton1.getInstance();
        //判断获取到的两个对象是不是同一个对象
        System.out.println(instance == instance1);
    }
}
