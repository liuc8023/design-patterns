package com.design.creational.singleton.hungryhanstyle;

/**
 * 饿汉式单例模式测试类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 21:42
 * @since JDK1.8
 */
public class HungryHanStyleSingletonTest {
    public static void main(String[] args) {
        //创建HungryHanStyleSingleton类的对象
        HungryHanStyleSingleton instance = HungryHanStyleSingleton.getInstance();
        HungryHanStyleSingleton instance1 = HungryHanStyleSingleton.getInstance();
        //判断获取到的两个对象是不是同一个对象
        System.out.println(instance == instance1);
    }
}
