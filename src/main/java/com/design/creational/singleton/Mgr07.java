package com.design.creational.singleton;

/**
 * 静态内部类方式
 * JVM保证单例
 * 加载外部类时不会加载内部类，这样可以实现懒加载
 * 此方式比Mgr01更完美，不会出现Mgr01的问题，也没有线程安全的问题
 * 因为静态内部类方式，在Mgr07类被加载到内存的时候，其内部类不会被加载到内存，只有当
 * 调用getInstance方法时，静态内部类Mgr07Holder才会被加载，INSTANCE才会被实例化
 * 此方式推荐
 */
public class Mgr07 {
    private Mgr07(){}

    private static class Mgr07Holder{
        private final static Mgr07 INSTANCE = new Mgr07();
    }

    public static Mgr07 getInstance(){
        return Mgr07Holder.INSTANCE;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                //1) 对象相等则hashCode一定相等；
                //2) hashCode相等对象未必相等。（不同对象的hashcode是否一定不一样？是错误的，
                // hashcode本身就是个函数，是可以重载的，你完全可以写个函数总是返回固定值。
                // 但hashcode函数从设计要求上来说，要尽量保证：不同对象的hashcode不同。）
                System.out.println(Mgr07.getInstance().hashCode());
            }).start();
        }
    }
}
