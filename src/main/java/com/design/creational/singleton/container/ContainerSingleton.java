package com.design.creational.singleton.container;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 容器式单例
 * @author liuc
 */
public class ContainerSingleton {
    private static Map<String,Object> concurrentHashMap = new ConcurrentHashMap<String,Object>();
    public static Object getBean(String className){
        synchronized (concurrentHashMap){
            if (!concurrentHashMap.containsKey(className)) {
                Object obj = null;
                try {
                    Class clazz = Class.forName(className);
                    obj = clazz.getDeclaredConstructor().newInstance();
                    concurrentHashMap.put(className,obj);
                } catch (Exception e){
                    e.printStackTrace();
                }
                return obj;
            }else {
                return concurrentHashMap.get(className);
            }
        }
    }
}
