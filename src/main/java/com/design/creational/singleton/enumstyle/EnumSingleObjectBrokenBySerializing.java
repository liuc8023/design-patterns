package com.design.creational.singleton.enumstyle;

import java.io.*;

/**
 * 测试序列化能否破坏
 * @author liuc
 */
public class EnumSingleObjectBrokenBySerializing {
    public static void main(String[] args) {
        EnumSingleObject s1 = EnumSingleObject.getInstance();
        EnumSingleObject s2 = null;
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("EnumSingleObject.obj");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(s1);
            oos.flush();
            oos.close();
            oos.close();
            FileInputStream fis = new FileInputStream("EnumSingleObject.obj");
            ObjectInputStream ois = new ObjectInputStream(fis);
            s2 = (EnumSingleObject)ois.readObject();
            ois.close();
            fis.close();
            //输出为false
            System.out.println(s1 == s2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
