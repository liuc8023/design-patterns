package com.design.creational.singleton.enumstyle;

import java.lang.reflect.Constructor;

/**
 * 测试反射能否破坏
 * @author liuc
 */
public class EnumSingleObject2BrokenByReflect {
    public static void main(String[] args) {
        try {
            Class<?> clazz = EnumSingleObject2.class;
            //通过反射获取类的私有构造方法
            Constructor c = clazz.getDeclaredConstructor(String.class,int.class);
            //强制访问
            c.setAccessible(true);
            Object obj1 = c.newInstance();
            Object obj2 = c.newInstance();
            //输出false
            System.out.println(obj1 == obj2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
