package com.design.creational.singleton.enumstyle;

import java.io.Serializable;

/**
 * 枚举式单例1
 * @author liuc
 */
public class EnumSingleObject implements Serializable {
    private EnumSingleObject(){

    }
    enum  SingletonEnum{
        INSTANCE;
        private EnumSingleObject instance;
        private SingletonEnum (){
            instance = new EnumSingleObject();
        }

        public EnumSingleObject getInstance(){
            return  INSTANCE.instance;
        }
    }

    /**
     * 对外暴露一个可以获取EnumSingleObject对象的静态方法
     * @return
     */
    public static EnumSingleObject getInstance(){
        return SingletonEnum.INSTANCE.getInstance();
    }
    /**
     * 只需要添加这一个函数即可解决序列化破坏单例对象的问题
     * @return
     */
    private Object readResolve(){
        return SingletonEnum.INSTANCE.getInstance();
    }
}
