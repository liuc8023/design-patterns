package com.design.creational.singleton.enumstyle;

/**
 * 枚举方式单例模式
 * 枚举类实现单例模式是极力推荐的单例实现模式，因为枚举类型是线程安全的，并且只会装载一次，设计者充分的利用了
 * 枚举的这个特性来实现单例模式，枚举的写法非常简单，而且枚举类型是所有单例实现中唯一一种不会被破坏的单例实现模式。
 * 另外说明：枚举方式属于饿汉式方式；在不考虑浪费内存空间的情况下首选枚举方式单例模式
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/31 16:50
 * @since JDK1.8
 */
public enum EnumStyleSingleton {
    INSTANCE;
}
