package com.design.creational.singleton.enumstyle;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 测试序列化能否破坏
 * @author liuc
 */
public class EnumSingleObject2BrokenBySerializing {
    public static void main(String[] args) {
        EnumSingleObject2 s1 = EnumSingleObject2.getInstance();
        EnumSingleObject2 s2 = null;
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("EnumSingleObject2.obj");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(s1);
            oos.flush();
            oos.close();
            oos.close();
            FileInputStream fis = new FileInputStream("EnumSingleObject2.obj");
            ObjectInputStream ois = new ObjectInputStream(fis);
            s2 = (EnumSingleObject2)ois.readObject();
            ois.close();
            fis.close();
            //输出为true
            System.out.println(s1 == s2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
