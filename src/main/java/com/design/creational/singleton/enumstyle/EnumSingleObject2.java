package com.design.creational.singleton.enumstyle;

import java.io.Serializable;

/**
 * 枚举式单例2
 * @author liuc
 */
public enum EnumSingleObject2 implements Serializable {
    INSTANCE;
    private Object data;

    public Object getData(){
        return data;
    }
    public void setData(Object data){
        this.data = data;
    }

    /**
     * 对外暴露一个可以获取EnumSingleObject2对象的静态方法
     * @return
     */
    public static EnumSingleObject2 getInstance(){
        return INSTANCE;
    }

}
