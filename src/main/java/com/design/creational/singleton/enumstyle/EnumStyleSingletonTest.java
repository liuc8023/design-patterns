package com.design.creational.singleton.enumstyle;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/10/31 16:51
 * @since JDK1.8
 */
public class EnumStyleSingletonTest {
    public static void main(String[] args) {
        EnumStyleSingleton instance = EnumStyleSingleton.INSTANCE;
        EnumStyleSingleton instance1 = EnumStyleSingleton.INSTANCE;
        System.out.println(instance == instance1);
    }
}
