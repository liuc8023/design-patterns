package com.design.creational.singleton.staticinnerclassstyle;

import java.lang.reflect.Constructor;

/**
 * 解决利用反射破坏单例问题测试
 * @author liuc
 */
public class SingletonBrokenByReflect1 {
    public static void main(String[] args) {
        try {
            Class<?> clazz = LazyStaticInnerClassSingleton1.class;
            //通过反射获取类的私有构造方法
            Constructor c = clazz.getDeclaredConstructor();
            //强制访问
            c.setAccessible(true);
            Object obj1 = c.newInstance();
            Object obj2 = c.newInstance();
            //输出false
            System.out.println(obj1 == obj2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
