package com.design.creational.singleton.staticinnerclassstyle;

/**
 * 静态内部类方式
 */
public class LazyStaticInnerClassSingleton {
    /**
     * 私有构造方法
     */
    private LazyStaticInnerClassSingleton(){

    }

    /**
     * 提供公共的访问方式
     * @return
     */
    public static final LazyStaticInnerClassSingleton getInstance(){
        return LazyHolder.LAZY;
    }

    /**
     * 静态内部类，未被使用时，是不会被加载的
     */
    private static class LazyHolder{
        private static final LazyStaticInnerClassSingleton LAZY = new LazyStaticInnerClassSingleton();
    }
}
