package com.design.creational.singleton.staticinnerclassstyle;

/**
 * 静态内部类方式
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/22 22:04
 * @since JDK1.8
 */
public class Singleton {
    /**
     * 私有构造方法
     *
     * @param
     * @return
     * @throws
     * @author liuc
     * @date 2021/10/22 22:06
     */
    private Singleton() {
    }

    /**
     * 提供公共的访问方式
     *
     * @author liuc
     * @date 2021/10/22 22:08
     * @version V1.0
     * @since JDK1.8
     */
    public static Singleton getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * 定义一个静态内部类
     *
     * @author liuc
     * @version V1.0
     * @date 2021/10/22 22:07
     * @since JDK1.8
     */
    private static class SingletonHolder {
        private static final Singleton INSTANCE = new Singleton();
    }
}
