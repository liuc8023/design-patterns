package com.design.creational.singleton.staticinnerclassstyle;

/**
 * 静态内部类方式，解决利用反射破坏单例的案例
 */
public class LazyStaticInnerClassSingleton1 {
    /**
     * 私有构造方法
     */
    private LazyStaticInnerClassSingleton1(){
        if (LazyHolder.LAZY != null) {
            throw new RuntimeException("实例被重复创建");
        }
    }

    /**
     * 提供公共的访问方式
     * @return
     */
    public static final LazyStaticInnerClassSingleton1 getInstance(){
        return LazyStaticInnerClassSingleton1.LazyHolder.LAZY;
    }

    /**
     * 静态内部类，未被使用时，是不会被加载的
     */
    private static class LazyHolder{
        private static final LazyStaticInnerClassSingleton1 LAZY = new LazyStaticInnerClassSingleton1();
    }
}
