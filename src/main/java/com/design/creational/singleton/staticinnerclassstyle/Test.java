package com.design.creational.singleton.staticinnerclassstyle;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/10/22 22:09
 * @since JDK1.8
 */
public class Test {
    public static void main(String[] args) {
        Singleton instance = Singleton.getInstance();
        Singleton instance1 = Singleton.getInstance();
        System.out.println(instance == instance1);
    }
}
