package com.design.creational.singleton.staticinnerclassstyle;

import java.lang.reflect.Constructor;

/**
 * 利用反射破坏单例
 */
public class SingletonBrokenByReflect {
    public static void main(String[] args) {
        try {
            Class<?> clazz = LazyStaticInnerClassSingleton.class;
            //通过反射获取类的私有构造方法
            Constructor c = clazz.getDeclaredConstructor();
            //强制访问
            c.setAccessible(true);
            Object obj1 = c.newInstance();
            Object obj2 = c.newInstance();
            //输出false
            System.out.println(obj1 == obj2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
