package com.design.creational.singleton.lazystyle;

/**
 * 验证多线程情况下，懒汉式不安全的情况
 */
public class LazyStyleSingletonThreadTest implements Runnable{
    public static void main(String[] args) {
        LazyStyleSingletonThreadTest threadTest = new LazyStyleSingletonThreadTest();
        for (int i = 0; i < 10; i++) {
            Thread t1 = new Thread(threadTest);
            t1.start();
        }
    }

    @Override
    public void run() {
        LazyStyleSingleton instance = LazyStyleSingleton.getInstance();
        System.out.println(instance);
    }
}
