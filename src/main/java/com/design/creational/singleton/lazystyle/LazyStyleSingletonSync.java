package com.design.creational.singleton.lazystyle;

/**
 * 懒汉式单例模式(线程安全)
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/20 21:53
 * @since JDK1.8
 */
public class LazyStyleSingletonSync {
    /**
     * 申明LazyStyleSingletonSync类型的变量instance
     */
    private static LazyStyleSingletonSync instance;

    /**
     * 私有构造方法
     *
     * @param
     * @return
     * @throws
     * @author liuc
     * @date 2021/10/20 21:43
     */
    private LazyStyleSingletonSync() {
    }

    /**
     * 对外提供访问方式
     * 此形式线程是安全的，但是锁的粒度有点大，会造成很大的性能开销
     * @param
     * @return com.design.creational.singleton.lazystyle.LazyStyleSingletonSync
     * @throws
     * @author liuc
     * @date 2021/10/20 21:43
     */
    public static synchronized LazyStyleSingletonSync getInstance() {
        //判断instance是否为null，如果为null，说明还没有创建LazyStyleSingleton类的对象
        //如果没有，就创建一个并返回，如果有，就直接返回
        if (instance == null) {
            instance = new LazyStyleSingletonSync();
        }
        return instance;
    }
}
