package com.design.creational.singleton.lazystyle;

/**
 * 懒汉式单例模式(双重检查锁)
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/20 22:01
 * @since JDK1.8
 */
public class LazyStyleSingletonDoubleCheck {
    /**
     * 申明LazyStyleSingletonDoubleCheck类型的变量
     * 加上volatile修饰变量，避免指令重排导致线程不安全
     */
    private static volatile LazyStyleSingletonDoubleCheck instance;

    /**
     * 私有构造方法
     *
     * @param
     * @return
     * @throws
     * @author liuc
     * @date 2021/10/20 22:02
     */
    private LazyStyleSingletonDoubleCheck() {
    }

    /**
     * 对外提供公共的访问方式
     *
     * @param
     * @return com.design.creational.singleton.lazystyle.LazyStyleSingletonDoubleCheck
     * @throws
     * @author liuc
     * @date 2021/10/20 22:03
     */
    public static LazyStyleSingletonDoubleCheck getInstance() {
        //第一次判断，如果instance的值不为null，不需要抢占锁，直接返回对象
        if (instance == null) {
            synchronized (LazyStyleSingletonDoubleCheck.class) {
                //第二次判断
                if (instance == null) {
                    instance = new LazyStyleSingletonDoubleCheck();
                }
            }
        }
        return instance;
    }
}
