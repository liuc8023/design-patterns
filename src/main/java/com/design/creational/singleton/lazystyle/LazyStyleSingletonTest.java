package com.design.creational.singleton.lazystyle;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/10/20 21:49
 * @since JDK1.8
 */
public class LazyStyleSingletonTest {
    public static void main(String[] args) {
        //创建LazyStyleSingleton类的对象
        LazyStyleSingleton instance = LazyStyleSingleton.getInstance();
        LazyStyleSingleton instance1 = LazyStyleSingleton.getInstance();
        //判断获取到的两个对象是不是同一个对象
        System.out.println(instance == instance1);
    }
}
