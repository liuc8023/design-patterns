package com.design.creational.singleton.lazystyle;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/10/20 22:06
 * @since JDK1.8
 */
public class LazyStyleSingletonDoubleCheckTest {
    public static void main(String[] args) {
        //创建LazyStyleSingletonDoubleCheck类的对象
        LazyStyleSingletonDoubleCheck instance = LazyStyleSingletonDoubleCheck.getInstance();
        LazyStyleSingletonDoubleCheck instance1 = LazyStyleSingletonDoubleCheck.getInstance();
        //判断获取到的两个对象是不是同一个对象
        System.out.println(instance == instance1);
    }
}
