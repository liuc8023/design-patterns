package com.design.creational.singleton.lazystyle;

/**
 * 懒汉式单例模式(非线程安全)
 *
 * @author liuc
 * @version V1.0
 * @date 2021/10/10 21:35
 * @since JDK1.8
 */
public class LazyStyleSingleton {
    /**
     * 申明LazyStyleSingleton类型的变量instance
     */
    private static LazyStyleSingleton instance;

    /**
     * 私有构造方法
     *
     * @param
     * @return
     * @throws
     * @author liuc
     * @date 2021/10/20 21:43
     */
    private LazyStyleSingleton() {
    }

    /**
     * 对外提供访问方式
     *
     * @param
     * @return com.design.creational.singleton.lazystyle.LazyStyleSingleton
     * @throws
     * @author liuc
     * @date 2021/10/20 21:43
     */
    public static LazyStyleSingleton getInstance() {
        //判断instance是否为null，如果为null，说明还没有创建LazyStyleSingleton类的对象
        //如果没有，就创建一个并返回，如果有，就直接返回
        if (instance == null) {
            instance = new LazyStyleSingleton();
        }
        return instance;
    }
}
