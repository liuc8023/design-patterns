package com.design.creational.singleton;

import java.util.concurrent.TimeUnit;

/**
 * lazy loading
 * 懒汉式
 * 虽然达到了按需初始化的目的（即谁用谁初始化），但是却带来了线程不安全的问题，
 * 如何解决线程安全的问题呢？
 *     可以通过加synchronized解决，但也带来效率下降
 */
public class Mgr04 {
    private static Mgr04 INSTANCE;

    private Mgr04(){}

    /**
     * 当使用了static修饰了同步方法之后，synchronized锁的不是当前实例对象（即this），
     * Mgr04.class文件。当一个线程获得了锁，它将会锁住Mgr04.class对象，
     * 其他线程无法访问Mgr04.class创建出来的其他实例对象所持有的静态同步方法，
     * 直到这个线程释放了锁之后，其他线程才可以获得锁。
     * @return
     */
    public static synchronized Mgr04 getInstance(){
        if (INSTANCE == null){
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            INSTANCE = new Mgr04();
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                //1) 对象相等则hashCode一定相等；
                //2) hashCode相等对象未必相等。（不同对象的hashcode是否一定不一样？是错误的，
                // hashcode本身就是个函数，是可以重载的，你完全可以写个函数总是返回固定值。
                // 但hashcode函数从设计要求上来说，要尽量保证：不同对象的hashcode不同。）
                System.out.println(Mgr04.getInstance().hashCode());
            }).start();
        }
    }
}
