package com.design.behavior.strategy.five;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/10 19:44
 * @since JDK1.8
 */
public class Test {
    public static void main(String[] args) {
        double total = 0.0d;
        double price = 300d;
        double num = 3;
        CashContext cc = new CashContext("正常收费");
        System.out.println("单价:"+price+" 数量:"+num+" 合计:"+cc.getResult(price*num));
        CashContext cc1 = new CashContext("满300返100");
        System.out.println("单价:"+price+" 数量:"+num+" 合计:"+cc1.getResult(price*num));
        CashContext cc2 = new CashContext("打八折");
        System.out.println("单价:"+price+" 数量:"+num+" 合计:"+cc2.getResult(price*num));
    }

}
