package com.design.behavior.strategy.five;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/9 20:48
 * @since JDK1.8
 */
public class CashNormal extends CashSuper {

    @Override
    public double acceptCash(double money) {
        return money;
    }
}
