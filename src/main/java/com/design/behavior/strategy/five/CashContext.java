package com.design.behavior.strategy.five;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/10 16:11
 * @since JDK1.8
 */
public class CashContext {
    private CashSuper cs;
    public CashContext(String type){
        switch (type){
            case "正常收费":
                cs =  new CashNormal();
                break;
            case "满300返100":
                cs = new CashReturn("300","100");
                break;
            case "打八折":
                cs = new CashRebate("0.8");
                break;
        }
    }

    public double getResult(double money){
        return cs.acceptCash(money);
    }
}
