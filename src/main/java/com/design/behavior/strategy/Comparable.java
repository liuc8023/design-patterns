package com.design.behavior.strategy;

public interface Comparable<T> {
    int compareTo(T o);
}
