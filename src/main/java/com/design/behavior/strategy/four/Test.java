package com.design.behavior.strategy.four;

import com.design.behavior.strategy.three.CashFactory;
import com.design.behavior.strategy.three.CashSuper;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/10 19:44
 * @since JDK1.8
 */
public class Test {
    public static void main(String[] args) {
        double total = 0.0d;
        double price = 300d;
        double num = 3;
        CashContext cc = getCashContext("正常收费");
        System.out.println("单价："+price+" 数量："+num+" 合计："+cc.getResult(price*num));
        CashContext cc1 = getCashContext("满300返100");
        System.out.println("单价："+price+" 数量："+num+" 合计："+cc1.getResult(price*num));
        CashContext cc2 = getCashContext("打八折");
        System.out.println("单价："+price+" 数量："+num+" 合计："+cc2.getResult(price*num));
    }

    public static CashContext getCashContext(String type){
        CashContext cc = null;
        switch (type){
            case "正常收费":
                cc = new CashContext(new CashNormal());
                break;
            case "满300返100":
                cc = new CashContext(new CashReturn("300","100"));
                break;
            case "打八折":
                cc = new CashContext(new CashRebate("0.8"));
                break;
        }
        return cc;
    }

}
