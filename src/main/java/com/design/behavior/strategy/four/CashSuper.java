package com.design.behavior.strategy.four;

/**
 * 现金收取抽象类
 * @author liuc
 * @version V1.0
 * @date 2021/12/9 20:46
 * @since JDK1.8
 */
public abstract class CashSuper {
    /**
     * 现金收取超类的抽象方法，收取现金，参数为原价，返回为当前价
     * @param money
     * @return double
     * @author liuc
     * @date 2021/12/9 20:47
     * @throws
     */
    public abstract double acceptCash(double money);
}
