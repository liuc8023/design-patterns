package com.design.behavior.strategy.four;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/10 16:11
 * @since JDK1.8
 */
public class CashContext {
    private CashSuper cs;
    public CashContext(CashSuper cs){
        this.cs = cs;
    }

    public double getResult(double money){
        return cs.acceptCash(money);
    }
}
