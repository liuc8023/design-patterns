package com.design.behavior.strategy;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
//        int[] a ={9,2,3,5,7,1,4};
        Sorter sorter = new Sorter();
//        sorter.sort(a);
//        System.out.println(Arrays.toString(a));

        Cat[] cat = {new Cat(3,3),new Cat(5,5),new Cat(1,1)};
        sorter.sort(cat);
        System.out.println(Arrays.toString(cat));

        Dog[] dog = {new Dog(3),new Dog(5),new Dog(1)};
        sorter.sort(dog);
        System.out.println(Arrays.toString(dog));
    }
}
