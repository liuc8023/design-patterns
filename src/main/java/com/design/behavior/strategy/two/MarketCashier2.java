package com.design.behavior.strategy.two;

/**
 * 针对MarketCashier1做活动无法打折的问题，做了如下优化
 * 虽然比MarketCashier1灵活了，可以支持打折，但是重复代码太多price * num就得写好多遍，代码因考虑重构；
 * 不过这还不是最主要的，现在新的需求来了，商场活动升级了，需要满300减100的促销活动，那该如何处理？
 * @author liuc
 * @version V1.0
 * @date 2021/12/9 20:10
 * @since JDK1.8
 */
public class MarketCashier2 {
    public void cashier(double price,int num,String discountType){
        double total = 0d;
        switch (discountType){
            case "正常收费":
                total = price * num;
                break;
            case "打九折":
                total = price * num * 0.9;
                break;
            case "打八折":
                total = price * num * 0.8;
                break;

        }
        System.out.println("单价："+price+" 数量："+num+" 合计："+total);
    }

    public static void main(String[] args) {
        MarketCashier2 mc = new MarketCashier2();
        mc.cashier(12.5d,4,"正常收费");
        mc.cashier(12.5d,4,"打九折");
        mc.cashier(12.5d,4,"打八折");
    }
}
