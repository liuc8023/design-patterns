package com.design.behavior.strategy.one;

/**
 * 商场收银软件
 * 如果商场做活动，打折促销，每打一次折，就得修改一次代码重新部署，这种软件开发出来一点都不灵活
 * @author liuc
 * @version V1.0
 * @date 2021/12/9 19:53
 * @since JDK1.8
 */
public class MarketCashier1 {
    public void cashier(double price,int num){
        double total = price*num;
        System.out.println("单价："+price+" 数量："+num+" 合计："+total);
    }

    public static void main(String[] args) {
        MarketCashier1 mc = new MarketCashier1();
        mc.cashier(12.5d,4);
    }
}
