package com.design.behavior.strategy.demo;


import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Sorter sorter = new Sorter();
        Cat[] cat = {new Cat(3,3),new Cat(5,5),new Cat(1,1)};
        sorter.sort(cat,new CatWeightComparator());
        System.out.println(Arrays.toString(cat));
        //按猫的身高排序，谁高谁排前面
        sorter.sort(cat,new CatHeightComparator());
        System.out.println(Arrays.toString(cat));

        Dog[] dog = {new Dog(3),new Dog(5),new Dog(1)};
        sorter.sort(dog,new DogComparator());
        System.out.println(Arrays.toString(dog));
    }
}
