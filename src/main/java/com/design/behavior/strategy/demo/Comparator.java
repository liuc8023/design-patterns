package com.design.behavior.strategy.demo;

public interface Comparator<T> {
    int Compare(T o1,T o2);
}
