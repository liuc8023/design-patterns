package com.design.behavior.strategy.three;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/9 21:02
 * @since JDK1.8
 */
public class Test {
    public static void main(String[] args) {
        CashSuper cs = CashFactory.createCashAccept("正常收费");
        double price = 300d;
        double num = 3;
        System.out.println("单价："+price+" 数量："+num+" 合计："+cs.acceptCash(price*num));
        CashSuper cs1 = CashFactory.createCashAccept("满300返100");
        System.out.println("单价："+price+" 数量："+num+" 合计："+cs1.acceptCash(price*num));
        CashSuper cs2 = CashFactory.createCashAccept("打八折");
        System.out.println("单价："+price+" 数量："+num+" 合计："+cs2.acceptCash(price*num));
    }

}
