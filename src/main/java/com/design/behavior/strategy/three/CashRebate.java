package com.design.behavior.strategy.three;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/12/9 20:49
 * @since JDK1.8
 */
public class CashRebate extends CashSuper{
    private double moneyRebate = 1d;
    public CashRebate(String moneyRebate){
        this.moneyRebate = Double.valueOf(moneyRebate);
    }

    @Override
    public double acceptCash(double money) {
        return money * moneyRebate;
    }
}
