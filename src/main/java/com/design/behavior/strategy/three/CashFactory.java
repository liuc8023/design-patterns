package com.design.behavior.strategy.three;

/**
 * 现金收费工厂类
 *     虽然简单工厂模式能够解决打折和满返的问题，但这个模式只是解决对象的创建问题，而且由于工厂本身包括了所有的收费方式，
 * 商场是可能经常性的更改打折额度和返利额度，每次维护或扩展收费方式都要改动这个工厂，以致代码需要成功新编译部署，这真的是
 * 很糟糕的处理方式，所以用它不是最好的办法。
 * @author liuc
 * @version V1.0
 * @date 2021/12/9 20:57
 * @since JDK1.8
 */
public class CashFactory {
    public static CashSuper createCashAccept(String type){
        CashSuper cs = null;
        switch (type){
            case "正常收费":
                cs = new CashNormal();
                break;
            case "满300返100":
                cs = new CashReturn("300","100");
                break;
            case "打八折":
                cs = new CashRebate("0.8");
                break;
        }
        return cs;
    }
}
