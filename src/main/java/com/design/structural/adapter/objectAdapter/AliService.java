package com.design.structural.adapter.objectAdapter;

public interface AliService {
    void getUserInfo(String phone);
}
