package com.design.structural.adapter.objectAdapter;

public class TestAliAndWechatAdapter {
    public static void main(String[] args) {
        AliServiceImpl aliServiceImpl = new AliServiceImpl();
        WechatServiceImpl wechatServiceImpl = new WechatServiceImpl();
        AliAndWechatAdapter adapter = new AliAndWechatAdapter(aliServiceImpl,wechatServiceImpl);

        adapter.getUserInfo("15927161554","340822196404274857");
    }
}
