package com.design.structural.adapter.objectAdapter;

public interface WechatService {
    void getUserInfo(String phone,String certNo);
}
