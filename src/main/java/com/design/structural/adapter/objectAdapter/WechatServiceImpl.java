package com.design.structural.adapter.objectAdapter;

public class WechatServiceImpl implements WechatService{
    @Override
    public void getUserInfo(String phone,String certNo) {
        System.out.println("微信用户用户通过手机号："+phone+"\t和证件号："+certNo+"\t校验成功");
    }
}
