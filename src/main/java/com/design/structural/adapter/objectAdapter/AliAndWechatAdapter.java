package com.design.structural.adapter.objectAdapter;

/**
 * 适配器类
 */
public class AliAndWechatAdapter implements WechatService{
    private AliService aliService;
    private WechatService wechatService;

    public AliAndWechatAdapter(AliService aliService, WechatService wechatService) {
        this.aliService = aliService;
        this.wechatService = wechatService;
    }

    /**
     * 适配器服务既可以校验阿里用户又可以校验微信用户
     * @param phone
     * @param certNo
     */
    @Override
    public void getUserInfo(String phone,String certNo) {
        aliService.getUserInfo(phone);
        wechatService.getUserInfo(phone,certNo);
    }
}
