package com.design.structural.adapter.objectAdapter;

public class AliServiceImpl implements AliService{
    @Override
    public void getUserInfo(String phone) {
        System.out.println("阿里用户通过手机号："+phone+"\t校验成功");
    }
}
