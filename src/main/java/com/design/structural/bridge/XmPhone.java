package com.design.structural.bridge;

public class XmPhone extends Phone{
    public XmPhone(PhoneColor color){
        super.setColor(color);
    }
    @Override
    public void run() {
        color.useColor();
        System.out.println("小米手机");
    }
}
