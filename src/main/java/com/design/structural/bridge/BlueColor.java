package com.design.structural.bridge;

public class BlueColor implements PhoneColor{
    @Override
    public void useColor() {
        System.out.println("蓝色");
    }
}
