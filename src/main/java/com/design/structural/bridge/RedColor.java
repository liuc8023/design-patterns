package com.design.structural.bridge;

public class RedColor implements PhoneColor{
    @Override
    public void useColor() {
        System.out.println("红色");
    }
}
