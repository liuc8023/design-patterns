package com.design.structural.bridge;

/**
 * 抽象类 手机
 */
public abstract class Phone {
    /**
     * 通过组合的方式来桥接其他行为
     */
    protected PhoneColor color;

    public void setColor(PhoneColor color) {
        this.color = color;
    }

    /**
     * 手机的方法
     */
    public abstract void run();
}
