package com.design.structural.bridge;

public class YellowColor implements PhoneColor{
    @Override
    public void useColor() {
        System.out.println("黄色");
    }
}
