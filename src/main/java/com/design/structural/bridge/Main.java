package com.design.structural.bridge;

public class Main {
    public static void main(String[] args) {
        PhoneColor phoneColor = new BlueColor();
        HwPhone hwPhone = new HwPhone(phoneColor);
        hwPhone.run();
    }
}
