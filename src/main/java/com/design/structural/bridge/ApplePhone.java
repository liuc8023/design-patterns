package com.design.structural.bridge;

public class ApplePhone extends Phone{
    public ApplePhone(PhoneColor color){
        super.setColor(color);
    }
    @Override
    public void run() {
        color.useColor();
        System.out.println("苹果手机");
    }
}
