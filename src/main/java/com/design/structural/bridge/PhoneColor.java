package com.design.structural.bridge;

public interface PhoneColor {
    void useColor();
}
