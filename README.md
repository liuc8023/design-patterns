# design-patterns

java 23设计模式

[Java设计模式：23种设计模式全面解析](http://c.biancheng.net/design_pattern/)

## 包说明

```
|--com
|  |--design
|  |  |--principles #软件设计原则
|  |  |  |--openClosed #开闭原则
|  |  |  |--liskovSubstitution #里氏替换原则
|  |  |  |--dependenceInversion #依赖倒置原则
|  |  |  |--interfaceSegregation #接口隔离原则
|  |  |  |--leastKnowledge #迪米特法则(又叫作最少知识原则)
|  |  |  |--compositeReuse #合成复用原则
|  |  |--creational #创建型
|  |  |  |--abstractFactory #抽象工厂模式
|  |  |  |--builder #建造者模式
|  |  |  |--factory #工厂方法模式
|  |  |  |--prototype #原型模式
|  |  |  |--singleton #单例模式
|  |  |--structural #结构型
|  |  |  |--adapter #适配器模式
|  |  |  |--bridge #桥接模式
|  |  |  |--composite #组合模式
|  |  |  |--decorator #装饰模式
|  |  |  |--facade #外观模式
|  |  |  |--flyweight #享元模式
|  |  |  |--proxy #代理模式
|  |  |--behavior #行为型
|  |  |  |--chainOfResponsibility #责任链模式
|  |  |  |--command #命令模式
|  |  |  |--interpreter #解释器模式
|  |  |  |--iterator #迭代器模式
|  |  |  |--mediator #中介者模式
|  |  |  |--memento #备忘录模式
|  |  |  |--observer #观察者模式
|  |  |  |--state #状态模式
|  |  |  |--strategy #策略模式
|  |  |  |--templateMethod #模板方法模式
|  |  |  |--visitor #访问者模式
```
